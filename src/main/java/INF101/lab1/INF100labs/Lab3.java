package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        System.out.println(crossSum(4321));

    }

    public static void multiplesOfSevenUpTo(int n) {
        int number = 0;
        while (number < n) {
            number += 7;
            if (number <= n) {
                System.out.println(number); 
            }
        }
    }

    public static void multiplicationTable(int n) {
        for (int j = 1; j < n+1; j++) {
            String output = j + ": ";
            for (int i = 1; i < n+1; i++) {
              output+=j*i + " ";
        }
        System.out.println(output);
        } 
    }

    public static int crossSum(int num) {
        int remainder = num;
        int sum = 0;
        while (remainder > 0) {
            sum += remainder % 10;
            remainder = remainder / 10;
        }
        return sum;
    }

}