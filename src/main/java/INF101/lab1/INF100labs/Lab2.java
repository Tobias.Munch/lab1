package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        boolean verdi = isLeapYear(1900);
        System.out.println(verdi);

    }

    public static void findLongestWords(String word1, String word2, String word3) {
        String longest = word1;
        int length = word1.length();

        if (length < word2.length()){
            longest = word2;
            length = word2.length();
        }
        else if (length == word2.length()) {
            longest += ("\n" + word2);
        }

        if (length < word3.length()){
            longest = word3;
            length = word3.length();
        }
        else if (length == word3.length()) {
            longest += ("\n" + word3);
        }

        System.out.println(longest);
    }

    public static boolean isLeapYear(int year) {
        if ((year % 4 == 0 && year % 100 !=0) || year % 400 == 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num % 2 == 0  && num > 0) {
            return true;
        }
        else {
            return false; 
        }
    }

}

