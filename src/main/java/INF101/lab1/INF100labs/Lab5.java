package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<Integer> a2 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b2 = new ArrayList<>(Arrays.asList(47, 21, -30));
        addList(a2, b2);
        System.out.println(a2);
        
        
                        
        
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> returnList = new ArrayList<>(Arrays.asList());
        for (int c : list) {
            returnList.add(c*2);
        }
        return returnList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> returnList = new ArrayList<>(Arrays.asList());
        for (int c : list) {
            if (c != 3) {
                returnList.add(c);
            }    
        }
        return returnList;
        }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> reducedList = new ArrayList<>(Arrays.asList());
        for (int c : list) {
            if (reducedList.contains(c) == false){
                reducedList.add(c);
            }
        }
        return reducedList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        int aSize = a.size();
        for (int i = 0; i < aSize; i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }

}
