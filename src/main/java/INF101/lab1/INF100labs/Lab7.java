package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid1.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid1.add(new ArrayList<>(Arrays.asList(6, 8, 1)));
        
        boolean equalSums1 = allRowsAndColsAreEqualSum(grid1);
        System.out.println(equalSums1);

    }
        

    

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        boolean result = true;
        int sumRad = 0;
        //sjekker om alle radene blir det samme 
        for (int i = 0; i < grid.size(); i++) {
            int lastSumRad = sumRad;
            sumRad = 0;
            for (int j = 0; j < (grid.get(i)).size(); j++) {
                sumRad+=(grid.get(i)).get(j);
            }
            if (sumRad != lastSumRad && i != 0) {
                result = false;
            }
        }
        //Sjekker om alle kolonnene blir det samme
        int sumKol = 0;
        for (int kol = 0; kol < grid.size(); kol++) {
            int lastSumKol = sumKol;
            sumKol = 0;
            for (int rad = 0; rad < grid.size(); rad++) {
                sumKol+=(grid.get(rad)).get(kol);
            }
            if (sumKol != lastSumKol && kol != 0) {
                result = false;
            }
        }

        return result; 
    }

}