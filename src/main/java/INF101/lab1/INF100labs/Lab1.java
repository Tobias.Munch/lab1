package INF101.lab1.INF100labs;

import java.util.Scanner;

/**
 * Implement the methods task1, and task2.
 * These programming tasks was part of lab1 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/1/
 */
public class Lab1 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void task1() {
        String hilsen = "Hei, det er meg, datamaskinen.\n" + 
                        "Hyggelig å se deg her.\n" + 
                        "Lykke til med INF101!\n";
        System.out.println(hilsen);
    }

    public static void task2() {
        sc = new Scanner(System.in); // Do not remove this line
        String spørsmål1 = "Hva er ditt navn?";
        System.out.println(spørsmål1);
        String name = sc.nextLine();
    
        String spørsmål2 = "Hva er din adresse?";
        System.out.println(spørsmål2);
        String address = sc.nextLine();

        String spørsmål3 = "Hva er ditt postnummer og poststed?";
        System.out.println(spørsmål3);
        String postSted = sc.nextLine();

        
        String finalString = name +"s adresse er: \n \n" + name + "\n" + address + "\n" + postSted;
        System.out.println(finalString);
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine
        ();
        return userInput;
    }


}
